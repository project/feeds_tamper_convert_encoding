Tamper Convert Encoding

Tamper Convert Encoding provides a Tamper plugin to convert text from one encoding to another before it gets saved.

Dependencies
- Tamper
